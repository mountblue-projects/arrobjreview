var fs = require('fs');

const questionTagData = require("./questionTags.json");
const tagsData = require("./tags.json");

// function getFileData(fileName) {
//     const getFileDataPromise = new Promise((resolve, reject) => {
//         fs.readFile(fileName, (err) => {
//             if (err) {
//                 reject(err);
//             }
//             else {
//                 resolve();
//             }
//         });
//     })
//     return getFileDataPromise;
// }

// async function getfile(fileName) {
//     try {
//         const data = await getFileData(fileName);
//         console.log(data)

//     } catch (error) {
//         throw error;
//     }
// }

// getFile("./tags.json");

result = {};


for (const qt of questionTagData) {
    const currentTag = qt["tagId"];
    // const currentId = ((qt["questionId"].split("-"))[1]);

    if (result[currentTag] === undefined) {
        result[currentTag] = 0;
    }
    result[currentTag] += 1;

}


let highestTag = 0;
let currentHighestTag = "";
for (const eachTag in result) {
    // console.log(result[eachTag]);
    if (highestTag < result[eachTag]) {
        highestTag = result[eachTag];
        currentHighestTag = eachTag;
    }
}
// console.log(currentHighestTag, highestTag);

let ans = {}

for (const tag of tagsData) {
    if (tag["id"] == currentHighestTag) {
        ans[tag["id"]] = tag["name"];
    }
}
console.log(ans);